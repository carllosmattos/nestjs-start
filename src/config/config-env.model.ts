import { IsEnum, IsNumber, IsPositive, IsString } from 'class-validator';

export enum NodeEnv {
  Development = 'development',
  Production = 'production',
  Test = 'test',
}

export class ConfigEnv {
  @IsNumber()
  @IsPositive()
  httpPort: number;

  @IsString()
  @IsEnum(NodeEnv)
  nodeEnv: string;

  @IsString()
  dbHost: string;

  @IsNumber()
  @IsPositive()
  dbPort: number;

  @IsString()
  dbUsername: string;

  @IsString()
  dbPassword: string;

  @IsString()
  dbDatabase: string;

  @IsString()
  dbConnection: 'mysql';

  @IsString()
  secret: string;

  @IsNumber()
  @IsPositive()
  expiresIn: number;

  get isProduction(): boolean {
    return this.nodeEnv === NodeEnv.Production;
  }
}
