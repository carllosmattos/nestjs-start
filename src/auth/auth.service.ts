import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, password: string): Promise<any> {
    try {
      const user: CreateUserDto = await this.userService.findOneByEmail(email);
      if (!user) {
        throw new BadRequestException('Usuário não encontrado');
      }

      const isMatch = await bcrypt.compare(password, user?.password);
      if (!isMatch) {
        throw new UnauthorizedException('Senha incoreta');
      }
      const payload = { sub: user.id, username: user.name, email };
      return {
        access_token: await this.jwtService.signAsync(payload),
      };
    } catch (error) {
      if (error.status === 401) {
        throw new UnauthorizedException('Senha incoreta');
      }
      return error;
    }
  }
}
