import { ConfigService } from 'src/config/config.service';

const configService = new ConfigService();
export const jwtConstants = {
  secret: configService.envConfig.secret,
  expiresIn: configService.envConfig.expiresIn,
};
