import { Document } from '../entities/user.entity';

export class CreateUserDto {
  id: number;
  name: string;
  email: string;
  documentType: Document;
  documentNumber: string;
  password: string;
}
export class ResponseUserDto {
  id: number;
  name: string;
  email: string;
  documentType: Document;
  documentNumber: string;
}
