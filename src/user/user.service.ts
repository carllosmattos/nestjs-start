import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { CreateUserDto, ResponseUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY')
    private userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<ResponseUserDto> {
    try {
      const hash = await bcrypt.hash(createUserDto.password, 10);
      const createUser = { ...createUserDto, password: hash };
      const user = await this.userRepository.save(createUser);
      delete user['password'];

      return user;
    } catch (error) {
      if (error.code.includes('ER_DUP_ENTRY')) {
        throw new BadRequestException('Documento ou email já cadastrado');
      }
      return error;
    }
  }

  async findAll(): Promise<Array<ResponseUserDto>> {
    try {
      const responseUsers = await this.userRepository.find();
      const users: Array<ResponseUserDto> = [];
      responseUsers.map((user: CreateUserDto) => {
        delete user['password'];
        users.push({ ...user });
      });
      return users;
    } catch (error) {
      return error;
    }
  }

  async findOneByEmail(email: string) {
    try {
      return await this.userRepository.findOne({
        where: { email },
      });
    } catch (error) {
      return error;
    }
  }

  async update(documentNumber: string, updateUserDto: UpdateUserDto) {
    try {
      if (updateUserDto.password) {
        const hash = await bcrypt.hash(updateUserDto.password, 10);
        updateUserDto.password = hash;
      }

      await this.userRepository
        .createQueryBuilder()
        .update(User)
        .set(updateUserDto)
        .where('documentNumber = :documentNumber', { documentNumber })
        .execute();

      return 'Dados atualizados com sucesso!';
    } catch (error) {
      return error;
    }
  }

  async remove(id: number) {
    try {
      await this.userRepository
        .createQueryBuilder('user')
        .delete()
        .where('id = :id', { id })
        .execute();

      return 'Usuário excluído com sucesso!';
    } catch (error) {
      return error;
    }
  }
}
